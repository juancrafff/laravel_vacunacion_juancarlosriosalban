<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use App\Models\Vacuna;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PacienteController extends Controller
{
    public function vacunar(Paciente $paciente)
    {
        $paciente = Paciente::find($paciente->id);
        $paciente->vacunado = true;
        $paciente->fechaVacuna = Carbon::now();
        $paciente->save();
        return back()->with("mensaje", "¡$paciente->nombre, vacunado correctamente!");
    }
}
