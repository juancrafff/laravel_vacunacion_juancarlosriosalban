<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Grupo;

class Vacuna extends Model
{
    protected $table = 'vacunas';
    use HasFactory;

    public function grupos()
    {
        return $this->belongsToMany(Grupo::class);
    }
    
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
