<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Grupo;

class Paciente extends Model
{
    protected $table = 'pacientes';
    use HasFactory;

    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    public function grupo()
    {
        return $this->belongsTo(Grupo::class);
    }
}
