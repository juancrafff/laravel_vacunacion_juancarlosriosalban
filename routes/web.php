<?php

use App\Http\Controllers\PacienteController;
use App\Http\Controllers\VacunaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [VacunaController::class, 'index']);
Route::get('vacunas', [VacunaController::class, 'index'])->name('vacunas.index');
Route::get('vacunas/{vacuna}',[VacunaController::class,'show'])->name('vacunas.show');
Route::put('pacientes/{paciente}/vacunar',[PacienteController::class,'vacunar'])->name('pacientes.vacunar');

Route::post('api/vacunas/crear', [VacunaController::class, 'crear']);
Route::get('api/vacunas/$idPaciente', [VacunaController::class, 'vacunas']);