@extends('layouts.master')
@section('titulo')
    Vacunas
@endsection
@section('contenido')
    <div class="row">
        @foreach ($vacunas as $clave => $vacuna)
        <div class="col-md-4">
			<a class="btn btn-outline-danger" href="{{ route('vacunas.show' , $vacuna) }}" style="text-decoration: none; color: inherit;">
				<div class="card" style="height:12em">
							<h4 class="card-title">{{$vacuna->nombre}}</h4> 
							<div class="card-block">
                                <h5>Posibles grupos de vacunación</h5>
								<ul>
                                    
                                    @foreach ($vacuna->grupos as $grupo)
                                        <li>{{$grupo->nombre}}</li>
                                    @endforeach
								</ul>
							</div>
				</div>
			</a>
		</div>
        @endforeach
    </div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/estilo.css') }}">
@endsection
