@extends('layouts.master')
@section('titulo')
    Vacuna {{$vacuna->nombre}}
@endsection
@section('contenido')
<div class="conatiner">
    <h3 class="text-center text-uppercase">{{$vacuna->nombre}}</h3>
    @if (Session::has('mensaje'))
    <h2 class="bg-success">
        {!! \Session::get('mensaje') !!}
    </h2>
    @endif
    <h4 class="text-uppercase">Pacientes no vacunados</h4>

    <div id="pacientesVacunados">
        <table class="table table-striped table-dark" style="width: 50%; margin: auto;">
			<thead>
				<tr>
                    <th>Nombre</th>
                    <th>Grupo de vacunación</th>
                    <th>Prioridad</th>
                    <th>Accion</th>
				</tr>
			</thead>
			<tbody>
                 @foreach ($vacuna->grupos as $grupo)
                 @foreach ($grupo->pacientes as $paciente)
                         @if ($paciente->vacunado == false)
                     <tr>
                             <td>{{$paciente->nombre}} {{$paciente->vacunado}}</td>
                             <td>{{$grupo->nombre}}</td>
                             <td>{{$grupo->prioridad}}</td>
                             <td>
                                 <form action="{{ route('pacientes.vacunar', [$paciente, $vacuna]) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="btn btn-primary">Vacunar</button>
                                </form>
                            </td>
                     </tr>
                         @endif
                 @endforeach
                 @endforeach
			</tbody>
		</table>
    </div>
        <h4 class="text-uppercase">Pacientes vacunados</h4>
    <div class="row">
        @foreach ($vacuna->grupos as $grupo)
        <div class="col-md-4">
            <table class="table table-striped table-dark">
                <thead>
                    <tr>
                        <th class="text-uppercase">{{$grupo->nombre}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($grupo->pacientes as $paciente)
                    @if ($paciente->vacunado == true)
                                            <tr>
                        <td>{{$paciente->nombre}}</td>
                    </tr>
                    @endif

                    @endforeach
                </tbody>
            </table>
								
		</div>
        @endforeach
    </div>
    </div>
</div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/estilo.css') }}">
@endsection
